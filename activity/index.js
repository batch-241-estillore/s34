const express = require("express");

const app = express();

const port = 3000;


app.use(express.json());

app.use(express.urlencoded({extended: true}));

app.get("/home", (request,response) => {
	response.send("Welcome to the home page")
})
//mock database
let users = [

	{
		"username": "gab",
		"password": "gab123"
	},
	{
		"username": "john",
		"password": "john123"
	},
	{
		"username": "jane",
		"password": "jane123"
	}
];

app.get("/users", (request,response) => {
	response.send(users)
});


app.delete("/delete-user", (request,response) => {
	for(let i = 0; i<users.length; i++){
		if(users[i].username == request.body.username){
			users.splice(i,1)
		}
	}
	response.send(`Successfully deleted ${request.body.username} from users`)
});



















app.listen(port, () => {
	console.log(`Server running at localhost: ${port}`)
})
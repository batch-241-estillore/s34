/*
REMEMBER THESE FOR INTITIALIZING PACKAGE AND NODE MODULE:
npm init -y
npm install express
touch .gitignore
npm install -g nodemon
*/


//require directive used to load packages like express, http among others
const express = require("express");

//create an application express
const app = express();

const port = 3000;

//Methods used from express JS are middlewares
//Middleware is software that provides common services and capabilities to applications outside of what's offered by the operating system

//Allow your app to read json data
app.use(express.json());

//Allow your app to read other data types because what we receive might be string or array
app.use(express.urlencoded({extended: true}));
//extended: true - by applying this option, it allows us to receive information in other data types


//[SECTION] Routes

//Express has methods corresponding to each HTTP method

//GET 
app.get("/greet", (request, response) => {
	//response.send sends a response back to the client
	response.send("Hello from the /greet endpoint");
});

//POST
app.post("/hello", (request, response) =>{
	response.send(`Hello there ${request.body.firstName} ${request.body.lastName}!`);
});

//mock database
let users = [];

//Simple registration form
app.post("/signup", (request, response) => {
	if(request.body.username !== '' && request.body.password !== ''){
		users.push(request.body);
		response.send(`User ${request.body.username} successfully registered!`);
	}else{
		response.send("Please input BOTH username and password.")
	}
});

//Change Password

app.patch("/change-password", (request,response) => {

	let message;

	for(let i = 0; i < users.length; i++){
		if(request.body.username == users[i].username){
			users[i].password = request.body.password;

			message = `User ${request.body.username}'s password has been updated.`;
			break;
		} else{
			message = "User does not exist.";
		}
	}
	response.send(message);
});


app.get("/getallusers", (request,response) => {
	response.send(users)
})























app.listen(port, () => console.log(`Server running at port: ${port}`));

